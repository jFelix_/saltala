import React from "react";
import { Switch } from "react-router-dom";
import AppliedRoute from "./components/AppliedRoute";

import LoginPage from "./Page/Login";
import RegisterPage from "./Page/Register";
import HomePage from "./Page/Home";
import TicketsPage from "./Page/Tickets";
import ShowTickets from "./Page/Tickets/show";
import EditTickets from "./Page/Tickets/edit";
import CreateTickets from "./Page/Tickets/create";


export default ({ childProps }) =>
  <Switch>
    <AppliedRoute path="/" exact component={HomePage} props={childProps}/>
    <AppliedRoute path="/login" exact component={LoginPage} props={childProps} />
    <AppliedRoute path="/signup" exact component={RegisterPage} props={childProps} />
    <AppliedRoute path="/tickets" exact component={TicketsPage} props={childProps} />
    <AppliedRoute path="/tickets/:id/show" exact component={ShowTickets} props={childProps} />
    <AppliedRoute path="/tickets/:id/edit" exact component={EditTickets} props={childProps} />
    <AppliedRoute path="/tickets/create" exact component={CreateTickets} props={childProps} />
  </Switch>;