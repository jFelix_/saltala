import React, { Component, Fragment } from "react";
import { Link } from "react-router-dom";
import { Nav, Navbar, NavItem } from "react-bootstrap";
import { LinkContainer } from "react-router-bootstrap";
import './App.css';
import Routes from "./Routes";


class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isAuthenticated: localStorage.getItem('isAuthenticated'),
      rol: localStorage.getItem('rol')
    };
  }

  userHasAuthenticated = authenticated => {
    this.setState({ isAuthenticated: authenticated });
    localStorage.setItem('isAuthenticated', authenticated);
  }
  userHasRol = rol => {
    this.setState({ rol: rol });
    localStorage.setItem('rol', rol);
  }
  handleLogout = event => {
    this.userHasAuthenticated(false);
    localStorage.removeItem('isAuthenticated');
    localStorage.removeItem('rol');
    
  }
  render() {
    const childProps = {
      isAuthenticated: this.state.isAuthenticated,
      userHasAuthenticated: this.userHasAuthenticated,
      rol: this.state.rol,
      userHasRol: this.userHasRol
    };
    return (
      <div className="App container">
        <Navbar fluid collapseOnSelect>
          <Navbar.Header>
            <Navbar.Brand>
              <Link to="/">Sáltala</Link>
            </Navbar.Brand>
            <Navbar.Toggle />
          </Navbar.Header>
          <Navbar.Collapse>
            <Nav pullRight>
              {this.state.rol === "Administrador" &&
                  <LinkContainer to="/tickets">
                    <NavItem>Tickets</NavItem>
                  </LinkContainer>
              }
              {this.state.isAuthenticated
                ?
                <LinkContainer to="/login">
                  <NavItem onClick={this.handleLogout}>Logout</NavItem>
                </LinkContainer>
                : <Fragment>
                  <LinkContainer to="/signup">
                    <NavItem>Registro</NavItem>
                  </LinkContainer>
                  <LinkContainer to="/login">
                    <NavItem>Login</NavItem>
                  </LinkContainer>
                </Fragment>
              }
            </Nav>
          </Navbar.Collapse>
        </Navbar>
        <Routes childProps={childProps} />
      </div>
    );
  }
}

export default App;
