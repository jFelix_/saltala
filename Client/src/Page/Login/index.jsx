import React, { Component } from "react";
import { Button, FormGroup, FormControl, ControlLabel, Alert, PageHeader } from 'react-bootstrap';
import './Login.css';
export default class LoginPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            mail: '',
            pass: '',
            error: ''
        };
    }

    validateForm() {
        return this.state.mail.length > 0 && this.state.pass.length > 0;
    }

    handleChange = event => {
        this.setState({
            [event.target.id]: event.target.value
        });
    }

    handleSubmit = event => {
        event.preventDefault();
        fetch('http://localhost:8080/api/login', {
            method: 'post',
            body: JSON.stringify({ mail: this.state.mail, pass: this.state.pass }),
            headers: {
                'Content-Type': 'application/json'
            }
        })
            .then(response => response.json())
            .then(data => {
                if (data.id) {
                    this.props.userHasAuthenticated(true);
                    this.props.userHasRol(data.rol);
                    this.props.history.push("/");
                } else {
                    this.setState({ error: data.msg });
                }
            }
            );
    }

    render() {
        return (
            <div className="Login">
                <PageHeader>Login</PageHeader>
                {this.state.error
                    && <Alert bsStyle="danger">
                        <strong>{this.state.error}</strong>
                    </Alert>
                }

                <form onSubmit={this.handleSubmit}>
                    <FormGroup controlId="mail" bsSize="large">
                        <ControlLabel>Email</ControlLabel>
                        <FormControl
                            autoFocus
                            type="email"
                            value={this.state.mail}
                            onChange={this.handleChange}
                        />
                    </FormGroup>
                    <FormGroup controlId="pass" bsSize="large">
                        <ControlLabel>Password</ControlLabel>
                        <FormControl
                            value={this.state.pass}
                            onChange={this.handleChange}
                            type="password"
                        />
                    </FormGroup>
                    <Button
                        block
                        bsSize="large"
                        bsStyle="primary"
                        disabled={!this.validateForm()}
                        type="submit"
                    >Login
                    </Button>
                </form>
            </div>
        );
    }
}