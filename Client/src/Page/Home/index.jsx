import React from 'react';
import { Redirect } from 'react-router';
export default class HomePage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isAuthenticated: localStorage.getItem('isAuthenticated'),
            rol: localStorage.getItem('rol')
            
        };

    }

    render() {
        return (
            this.state.isAuthenticated ?
                this.state.rol === 'Administrador' ?
                    <Redirect to="/tickets" /> :
                    this.state.rol === 'Usuario' ?
                     <p>Bienvenido Usuario, no posees ninguna acción por el momento, vuelve pronto</p>    
                    :
                        <Redirect to="/login" />
                :
                <Redirect to="/login" />
        );
    }
}