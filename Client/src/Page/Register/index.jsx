import React from 'react';
import { Alert, Button, FormGroup, FormControl, ControlLabel, PageHeader } from 'react-bootstrap';
import './Register.css';

export default class RegisterPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            nombre: '',
            mail: '',
            pass: '',
            submitted: false
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        this.setState({
            [event.target.id]: event.target.value
        });
    }

    handleSubmit(event) {
        event.preventDefault();

        this.setState({ submitted: true });

        fetch('http://localhost:8080/api/usuarios', {
            method: 'post',
            body: JSON.stringify({
                nombre: this.state.nombre,
                mail: this.state.mail,
                pass: this.state.pass
            }),
            headers: {
                'Content-Type': 'application/json'
            }
        })
            .then(response => response.json())
            .then(data => {
                if (data.id) {
                    this.props.userHasAuthenticated(true);
                    this.props.userHasRol(data.rol);
                    this.props.history.push("/");
                } else {
                    this.setState({ error: data.msg });
                }
            });
    }
    validateForm() {
        return this.state.mail.length > 0 && this.state.pass.length > 0 && this.state.nombre.length > 0;
    }

    render() {
        return (
            <div className="register">
                <PageHeader>Registro</PageHeader>
                <form onSubmit={this.handleSubmit}>
                {this.state.error
                    && <Alert bsStyle="danger">
                        <strong>{this.state.error}</strong>
                    </Alert>
                }
                    <FormGroup controlId="nombre" bsSize="large">
                        <ControlLabel>Nombre</ControlLabel>
                        <FormControl
                            autoFocus
                            type="text"
                            value={this.state.nombre}
                            onChange={this.handleChange}
                        />
                    </FormGroup>
                    <FormGroup controlId="mail" bsSize="large">
                        <ControlLabel>Email</ControlLabel>
                        <FormControl
                            autoFocus
                            type="email"
                            value={this.state.mail}
                            onChange={this.handleChange}
                        />
                    </FormGroup>
                    <FormGroup controlId="pass" bsSize="large">
                        <ControlLabel>Password</ControlLabel>
                        <FormControl
                            value={this.state.pass}
                            onChange={this.handleChange}
                            type="password"
                        />
                    </FormGroup>
                    <Button
                        block
                        bsStyle="primary"
                        bsSize="large"
                        disabled={!this.validateForm()}
                        type="submit"
                    >Registrarse
                    </Button>
                </form>
            </div>
        );
    }
}