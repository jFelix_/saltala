import React from 'react';
import { Button, Modal } from 'react-bootstrap';
export default class Delete extends React.Component {
    render() {
        return (
            <Modal {...this.props}>
                <Modal.Header closeButton>
                    <Modal.Title>Eliminar</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <p>
                        Estas Seguro que deseas eliminar el ticket {this.props.id}?.
              </p>
                </Modal.Body>
                <Modal.Footer>
                    <Button bsStyle="danger" onClick={this.props.onDelete}>Eliminar</Button>
                    <Button onClick={this.props.onHide}>Cerrar</Button>
                </Modal.Footer>
            </Modal>
        );
    }
}