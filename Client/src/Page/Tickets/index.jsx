import React from 'react';
import { Button, ButtonGroup, Glyphicon, Table } from 'react-bootstrap';
import DeleteModal from './delete';
import ModalGenerico from '../../components/Modal';
export default class Tickets extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isAuthenticated: localStorage.getItem('isAuthenticated'),
            rol: localStorage.getItem('rol'),
            isLoading: false,
            tickets: [],
            deleteShow: false,
            modalShow: false
        };
        this.state.rol !== 'Administrador' && this.props.history.push("/");
    }
    componentDidMount() {
        this.setState({ isLoading: true });
        fetch('http://localhost:8080/api/tickets')
            .then(response => response.json())
            .then(data => this.setState({ tickets: data, isLoading: false }));
    }
    nextPath(path) {
        this.props.history.push(path);
    }
    deleteTicket = () => {
        this.setState({ deleteShow: false });
        fetch('http://localhost:8080/api/tickets/' + this.state.id, {
            method: 'delete',
            headers: {
                'Content-Type': 'application/json'
            }
        })
            .then(response => response.json())
            .then(data => { this.setState({ modalShow: true }); this.componentDidMount() });
    }
    render() {
        const { tickets, isLoading } = this.state;
        if (isLoading) {
            return <p>Loading ...</p>;
        }
        let deleteClose = () => this.setState({ deleteShow: false });
        let modalClose = () => this.setState({ modalShow: false });
        return (
            <div>
                <Table responsive>
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>ticket pedido</th>
                            <th>Usuario</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        {tickets.map(ticket =>
                            <tr>
                                <td>{ticket.id}</td>
                                <td>{ticket.ticket_pedido}</td>
                                <td>{ticket.Usuario.nombre} ({ticket.Usuario.mail})</td>
                                <td>
                                    <ButtonGroup>
                                        <Button bsStyle="primary" onClick={() => this.nextPath('/tickets/' + ticket.id + '/edit')}><Glyphicon glyph="pencil" /></Button>
                                        <Button bsStyle="success" onClick={() => this.nextPath('/tickets/' + ticket.id+'/show')}><Glyphicon glyph="eye-open" /></Button>
                                        <Button bsStyle="danger" onClick={() => this.setState({ deleteShow: true, id: ticket.id })}><Glyphicon glyph="remove" /></Button>
                                    </ButtonGroup>
                                </td>
                            </tr>
                        )}
                    </tbody>
                    <tfoot>
                        <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th><Button bsStyle="primary" onClick={() => this.nextPath('/tickets/create')}><Glyphicon glyph="plus" /> Agregar Ticket</Button></th>
                        </tr>
                    </tfoot>
                </Table>
                <DeleteModal show={this.state.deleteShow} onHide={deleteClose} id={this.state.id} onDelete={this.deleteTicket} />
                <ModalGenerico show={this.state.modalShow} onHide={modalClose} title="Exito" text="Ticket borrado exitosamente" />
            </div>
        );
    }
}