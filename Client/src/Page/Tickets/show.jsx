import React from 'react';
import { Panel, PageHeader, Table } from 'react-bootstrap';
export default class Show extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isAuthenticated: localStorage.getItem('isAuthenticated'),
            rol: localStorage.getItem('rol'),
            isLoading: true,
            ticket: {}
        };
        this.state.rol !== 'Administrador' && this.props.history.push("/");
    }
    componentDidMount() {
        fetch('http://localhost:8080/api/tickets/' + this.props.match.params.id)
            .then(response => response.json())
            .then(data => this.setState({ ticket: data, isLoading: false }));
    }

    render() {
        const { ticket, isLoading } = this.state;
        console.log(ticket);
        if (isLoading) {
            return <p>Loading ...</p>;
        }
        if(ticket === null){
            return <p>Ticket no existe ...</p>;
        }
        return (
            <Panel>
                <Panel.Body>
                    <PageHeader>Detalle Ticket</PageHeader>
                    <Table striped bordered condensed hover>
                        <tbody>
                            <tr>
                                <th>Id</th>
                                <td>{ticket.id}</td>
                            </tr>
                            <tr>
                                <th>ticket_pedido</th>
                                <td>{ticket.ticket_pedido}</td>
                            </tr>
                            <tr>
                                <th>Usuario</th>
                                <td>
                                Nombre: {ticket.Usuario.nombre} <br/>Email: {ticket.Usuario.mail}
                                </td>
                            </tr>
                        </tbody>
                    </Table>
                </Panel.Body>
            </Panel>
        );
    }
}