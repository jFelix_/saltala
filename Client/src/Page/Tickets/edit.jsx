import React from 'react';
import { Button, Panel, PageHeader, FormGroup, FormControl, ControlLabel } from 'react-bootstrap';

export default class Create extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isAuthenticated: localStorage.getItem('isAuthenticated'),
            rol: localStorage.getItem('rol'),
            isLoading: true,
            ticket: {
                ticket_pedido: '',
                UsuarioId: '',
            },
            usuarios: []
        };
        this.state.rol !== 'Administrador' && this.props.history.push("/");
    }
    componentDidMount() {
        fetch('http://localhost:8080/api/tickets/' + this.props.match.params.id)
            .then(response => response.json())
            .then(data => this.setState({ ticket: {
                ticket_pedido: data.ticket_pedido,
                UsuarioId: data.id_user,
            }, isLoading: false }));
        fetch('http://localhost:8080/api/usuarios/')
            .then(response => response.json())
            .then(data => this.setState({ usuarios: data, isLoading: false }));
    }
    handleChange = event => {
        const { ticket } = this.state;
        const { id, value } = event.target;
        this.setState({
            ticket: {
                ...ticket,
                [id]: value
            }
        });
    }
    handleSelectChange = event => {
        const { ticket } = this.state;
        this.setState({
            ticket: {
                ...ticket,
                UsuarioId: this.inputEl.value
            }
        });
    }
    validateForm() {
        return this.state.ticket.ticket_pedido.length > 0;
    }
    handleSubmit = event => {
        event.preventDefault();
        fetch('http://localhost:8080/api/tickets/' + this.props.match.params.id, {
            method: 'put',
            body: JSON.stringify({
                ticketId: this.props.match.params.id,
                ticketPedido: this.state.ticket.ticket_pedido,
                id_tipouser: this.state.ticket.UsuarioId,
            }),
            headers: {
                'Content-Type': 'application/json'
            }
        })
            .then(response => response.json())
            .then(data => {this.props.history.push("/tickets");});
    }


    render() {
        const { isLoading } = this.state;
        const { ticket, usuarios } = this.state;
        if (isLoading) {
            return <p>Loading ...</p>;
        }
        
        return (
            <Panel>
                <Panel.Body>
                    <PageHeader>Crear Ticket</PageHeader>
                    <form onSubmit={this.handleSubmit}>
                        <FormGroup controlId="ticket_pedido" bsSize="large">
                            <ControlLabel>Ticket Pedido</ControlLabel>
                            <FormControl
                                autoFocus
                                type="text"
                                value={ticket.ticket_pedido}
                                onChange={this.handleChange}
                            />
                        </FormGroup>
                        <FormGroup controlId="UsuarioId">
                            <ControlLabel>Usuario</ControlLabel>
                            <FormControl
                                componentClass="select"
                                placeholder="select"
                                inputRef={el => this.inputEl = el}
                                onChange={this.handleSelectChange}>
                                defaultValue={this.state.ticket.UsuarioId}
                                <option value="Selecciona">Selecciona</option>
                                {usuarios.map(usuario =>
                                    <option value={usuario.id} selected={this.state.ticket.UsuarioId === usuario.id}>{usuario.mail}</option>
                                )}
                            </FormControl>
                        </FormGroup>
                        <Button
                            block
                            bsSize="large"
                            bsStyle="primary"
                            disabled={!this.validateForm()}
                            type="submit"
                        >Editar Ticket
                    </Button>
                    </form>
                </Panel.Body>
            </Panel>
        );
    }
}