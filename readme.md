# Sáltala Test #


## Prerequisites ##
1. node package manager installed 
2. Mysql database running in localhost in the default Mysql port.

## Running the app ##
1. clone the repo and cd into it
2. cd to Server and type the command `npm install` and hit enter to install the packages
3. cd to config and change DB config in config.js
4. type `node_modules/.bin/sequelize db:create` to create DB
5. type `node_modules/.bin/sequelize db:migrate` to migrate models to DB
6. type `node_modules/.bin/sequelize db:seed:all` to insert some data into a few tables
7. type `npm start` to run the server and open in browser.
8. cd to Client and type the command `npm install` and hit enter to run the app
9. type `npm start` to run the client and open in browser.

## Description ##
- Debes crear una app en React + Node.js (Express) + MySql en la cual se pueda registrar y logear usuario (sin recuperación de contraseña) y en la que existan dos perfiles de usuario: Administrador y Usuario. 
- El perfil administrador puede gestionar tickets (Crud).
- El login de usuarios debe discriminar y redireccionar según su perfil.
- Debes subir el proyecto a git y enviarnos las instrucciones para levantar el proyecto (incluye el script de SQL en el proyecto). 
- El proyecto  cuenta con 3 Tablas:
1. usuarios: id, id_tipouser, nombre, mail, pass
2. ticket: id , id_user , ticket_pedido
3. tipo_usuario:  id, nombre
