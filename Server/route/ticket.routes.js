module.exports = function(app) {
    const tickets = require('../controllers/tickets.controller.js');
    app.post('/api/tickets', tickets.create);
    app.get('/api/tickets', tickets.findAll);
    app.get('/api/tickets/:ticketId', tickets.findById);
    app.put('/api/tickets/:ticketId', tickets.update);
    app.delete('/api/tickets/:ticketId', tickets.delete);
}