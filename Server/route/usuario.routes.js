module.exports = function(app) {
    const usuarios = require('../controllers/usuario.controller.js');
    app.post('/api/login', usuarios.login);
    app.post('/api/usuarios', usuarios.create);
    app.get('/api/usuarios', usuarios.findAll);
    app.get('/api/usuarios/:usuarioId', usuarios.findById);
    app.put('/api/usuarios/:usuarioId', usuarios.update);
    app.delete('/api/usuarios/:usuarioId', usuarios.delete);
}