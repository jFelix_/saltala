'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'Tickets', // name of Source model
      'id_user', // name of the key we're adding 
      {
        type: Sequelize.INTEGER,
        references: {
          model: 'Usuarios', // name of Target model
          key: 'id', // key in Target model that we're referencing
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      }
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn(
      'Tickets', // name of Source model
      'id_user' // key we want to remove
    );
  }
};
