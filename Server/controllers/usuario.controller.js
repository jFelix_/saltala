var models = require('../models');
exports.create = (req, res) => {
    models.Usuario.create({
        nombre: req.body.nombre,
        mail: req.body.mail,
        pass: req.body.pass,
        id_tipouser: 2 //Por defecto Usuario
    }).then(usuario => {
        res.json(usuario);
    }).catch(function(err){
        console.log(err.errors[0].message);
        res.json({msg:err.errors[0].message});
    });
   
};

exports.findAll = (req, res) => {
    models.Usuario.findAll().then(usuarios => {
        res.json(usuarios);
    });
};

exports.findById = (req, res) => {
    models.Usuario.findById(req.params.usuarioId).then(usuario => {
        res.json(usuario);
    })
};
exports.login = (req, res) => {
    var mail = req.body.mail,
        password = req.body.pass;
    models.Usuario.findOne({
        where: { mail: mail },
        include: [{
            model: models.TipoUsuario
        }]
    }).then(function (usuario) {
        if (!usuario) {
            res.json({ msg: "Usuario Invalido" });
        } else if (!usuario.validPassword(password)) {
            res.json({ msg: "Usuario Invalido" });
        } else {
            res.json({
                id:usuario.dataValues.id, 
                nombre:usuario.dataValues.nombre, 
                mail:usuario.dataValues.mail, 
                rol:usuario.dataValues.TipoUsuario.nombre, 
            });
        }
    });
};

exports.update = (req, res) => {
    models.Usuario.update({ nombre: req.body.nombre, mail: req.body.mail, pass: req.body.pass },
        { where: { id: req.params.usuarioId } }
    ).then(() => {
        res.status(200).json({ msg: "Usuario " + req.params.usuarioId + 'actualizado correctamente' });
    });
};

// Delete a Customer by Id
exports.delete = (req, res) => {
    const id = req.params.usuarioId;
    models.Usuario.destroy({
        where: { id: id }
    }).then(() => {
        res.status(200).send({ msg: 'Usuario ' + id + 'eliminado' });
    });
};