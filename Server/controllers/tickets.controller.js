var models = require('../models');
exports.create = (req, res) => {
    models.Ticket.create({
        ticket_pedido: req.body.ticketPedido,
        id_user: parseInt(req.body.UsuarioId)
    }).then(ticket => {
        res.json(ticket);
    });
};

exports.findAll = (req, res) => {
    models.Ticket.findAll({
        include: [{
            model: models.Usuario
        }]
    }).then(tickets => {
        res.json(
            tickets
        )
    }).catch(console.error);
};

exports.findById = (req, res) => {
    models.Ticket.findById(req.params.ticketId,{
        include: [{
            model: models.Usuario
        }]
    }).then(ticket => {
        res.json(ticket);
    })
};

exports.update = (req, res) => {
    models.Ticket.update({ ticket_pedido: req.body.ticketPedido, id_tipouser: req.body.id_tipouser },
        { where: { id: req.params.ticketId } }
    ).then(() => {
        res.status(200).json({ msg: "Ticket " + req.params.ticketId + 'actualizado correctamente' });
    });
};

// Delete a Customer by Id
exports.delete = (req, res) => {
    const id = req.params.ticketId;
    models.Ticket.destroy({
        where: { id: id }
    }).then(() => {
        res.status(200).send({ msg: 'Ticket ' + id + 'eliminado' });
    });
};