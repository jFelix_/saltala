'use strict';
module.exports = (sequelize, DataTypes) => {
  var TipoUsuario = sequelize.define('TipoUsuario', {
    nombre: DataTypes.STRING
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return TipoUsuario;
};