'use strict';
module.exports = (sequelize, DataTypes) => {
  var Ticket = sequelize.define('Ticket', {
    ticket_pedido: DataTypes.STRING
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  Ticket.associate = models => {
    Ticket.belongsTo(models.Usuario, {foreignKey: 'id_user', targetKey: 'id'});
  };
  return Ticket;
};