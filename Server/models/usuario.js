'use strict';
import { genSaltSync, hashSync, compareSync } from 'bcrypt';
export default (sequelize, DataTypes) => {
  var Usuario = sequelize.define('Usuario', {
    nombre: DataTypes.STRING,
    mail: {
      type: DataTypes.STRING,
      unique: true,
      validate: {
        isEmail: true,
        isUnique: sequelize.validateIsUnique('mail')
      }
    },
    pass: DataTypes.STRING
  }, {
      hooks: {
        beforeCreate: (user) => {
          var salt = genSaltSync();
          user.pass = hashSync(user.pass, salt);
        }
      },
      instanceMethods: {
        validPassword: function (pass) {
          console.log(pass, this.pass);
          return compareSync(pass, this.pass);
        }
      }
    }
  );
  Usuario.associate = models => {
    Usuario.belongsTo(models.TipoUsuario, {foreignKey: 'id_tipouser', targetKey: 'id'});
  };
  return Usuario;
};