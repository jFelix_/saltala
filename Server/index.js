import app from './app';
import db from './models/index';
const { PORT = 8080 } = process.env;
// db.sequelize.sync({force: true}).then(() => {
//   console.log('Drop and Resync with { force: true }');
// });

app.listen(PORT, () => console.log(`Listening on port ${PORT}`));