'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('TipoUsuarios', [{
      nombre: 'Usuario'
    }], {});
},

down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('TipoUsuarios', null, {});
}
};
