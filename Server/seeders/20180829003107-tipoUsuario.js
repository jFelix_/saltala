'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
      return queryInterface.bulkInsert('TipoUsuarios', [{
        nombre: 'Administrador'
      }], {});
  },

  down: (queryInterface, Sequelize) => {
      return queryInterface.bulkDelete('TipoUsuarios', null, {});
  }
};
